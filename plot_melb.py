from bokeh.models import HoverTool
from bokeh.plotting import figure, show, output_file, ColumnDataSource

import json
import csv


def get_map(pedestrian_count):
    with open('/Users/edk/OneDrive/edk/mbs/02-business-analytics-foundations/programming/assignment1/Building_Foot_Prints.geojson') as file:
        data = json.load(file)
    lst = data['features']

    with open('/Users/edk/OneDrive/edk/mbs/02-business-analytics-foundations/programming/assignment1/Pedestrian_Sensor_Locations.csv') as sensor_file:
        sensor_reader = csv.reader(sensor_file)
        sensor_data = [line for line in sensor_reader]

    sensor_location = [(sensor_data[i][9], sensor_data[i][8])
                       for i in range(1, len(sensor_data))]

    sensor_description = [sensor_data[i][2]
                          for i in range(1, len(sensor_data))]

    # create source for buildings
    building_xs = []
    building_ys = []

    for building_id in range(0, len(lst)):
        x = []
        y = []
        for coord in lst[building_id]['geometry']['coordinates'][0][0]:
            x.append(coord[0])
            y.append(coord[1])

        building_xs.append(x)
        building_ys.append(y)

    source = ColumnDataSource(data=dict(
        x=building_xs,
        y=building_ys

    ))

    # create list of colours for sensors
    # colouring based on order of sensors, red for number 1 highest ped. count
    colors = ["#00ff00", "#add8e6", "#0000ff", "#00008b"]
    sorted_pedestrian_count = sorted(pedestrian_count.items(),
                                     key=lambda x: float(x[1]),
                                     reverse=True)
    sensor_colours = []
    max
    for i in range(len(sorted_pedestrian_count)):
        if i == 0:
            # appends a tuple, (sensor_id, colour)
            sensor_colours.append((sorted_pedestrian_count[i][0], colors[0]))
        else:
            sensor_colours.append((sorted_pedestrian_count[i][0], colors[2]))

    # re-order the list according to sensor_id
    sensor_colours = sorted(sensor_colours, key=lambda x: x[0])
    sensor_colours = [x[1] for x in sensor_colours]

    # create source for sensors
    sensor_xs = []
    sensor_ys = []
    for coord in sensor_location:
        sensor_xs.append(float(coord[0]))
        sensor_ys.append(float(coord[1]))

    sensor_ds = []
    for sensor in sensor_description:
        sensor_ds.append(sensor)

    sensor_source = ColumnDataSource(data=dict(
        x=sensor_xs,
        y=sensor_ys,
        name=sensor_ds,
        value=pedestrian_count,
        colour=sensor_colours
    ))

    TOOLS = "pan,wheel_zoom,box_zoom,reset,hover,save"

    p = figure(title="Melbourne CBD", tools=TOOLS)

    # draw patches for buildings
    p.patches('x', 'y', source=source,
              fill_color='white', fill_alpha=0.7,
              line_color="black", line_width=0.5)

    # draw circles for sensors
    p.circle('x', 'y', source=sensor_source,
             alpha=0.9,
             size=10,
             fill_color='colour'
             )

    # configure the mouse hover tooltip
    hover = p.select_one(HoverTool)
    hover.point_policy = "follow_mouse"
    hover.tooltips = [
        ("Location", "@name"),
        ("Count", "@value")
    ]

    output_file("/Users/edk/OneDrive/edk/mbs/02-business-analytics-foundations/programming/assignment1/dev/melb.html",
                title="melb.py example")

    show(p)
