
import pandas as pd
# library for loading pickled data
import pickle
# for multiple linear regression OLS
import statsmodels.formula.api as smf
from statsmodels.api import add_constant

# declare constants
# BECAUSE PICKLING STATSMODELS REGRESSIONRESULTS BREAK PREDICTION
DAYS_OF_WEEK = {}
DAYS_OF_WEEK['FRIDAY'] = [0, 0, 0, 0, 0, 0]
DAYS_OF_WEEK['MONDAY'] = [1, 0, 0, 0, 0, 0]
DAYS_OF_WEEK['SATURDAY'] = [0, 1, 0, 0, 0, 0]
DAYS_OF_WEEK['SUNDAY'] = [0, 0, 1, 0, 0, 0]
DAYS_OF_WEEK['THURSDAY'] = [0, 0, 0, 1, 0, 0]
DAYS_OF_WEEK['TUESDAY'] = [0, 0, 0, 0, 1, 0]
DAYS_OF_WEEK['WEDNESDAY'] = [0, 0, 0, 0, 0, 1]

'''
given a set of parameters, run the predict method for each model
to obtain the predicted pedestrian count for each sensor

returns a dictionary of sensor_id:count pairs
'''
def get_pedestrian_count(parameters):
    models = load_models()
    results = {}

    # build the dataframe for the parameter
    # predict() method takes in a dataframe
    data_frame = get_parameter_dataframe(parameters)

    for i in range(0, len(models)):
        results[i] = [models[i].predict(add_constant(data_frame[["Weekday[T.Monday]", "Weekday[T.Saturday]",
                                                                 "Weekday[T.Sunday]", "Weekday[T.Thursday]",
                                                                 "Weekday[T.Tuesday]", "Weekday[T.Wednesday]",
                                                                 "day_of_year", "Solar", "Temperature", "Rainfall"]]), transform=False)]
    return results

'''
method to load the linear regression models
returns a list of models, one for each sensor
'''
def load_models():
    pkl_file = open('models.pkl', 'rb')
    models = pickle.load(pkl_file)
    return models

'''
given a set of parameters, create a pandas dataframe

the dataframe is used as input/argument into the statsmodels.predict() method

returns a pandas dataframe
'''
def get_parameter_dataframe(params):
    t_params = {}
    key_names = ['Weekday[T.Monday]',
                 'Weekday[T.Saturday]',
                 'Weekday[T.Sunday]',
                 'Weekday[T.Thursday]',
                 'Weekday[T.Tuesday]',
                 'Weekday[T.Wednesday]']

    if DAYS_OF_WEEK[str.upper(params['weekday'])]:
        binary_representation = DAYS_OF_WEEK[str.upper(params['weekday'])]
        for i in range(0, len(key_names)):
            t_params[key_names[i]] = [binary_representation[i]]

        t_params['day_of_year'] = [int(params['day_of_year'])]
        t_params['Solar'] = [float(params['solar'])]
        t_params['Temperature'] = [float(params['temperature'])]
        t_params['Rainfall'] = [float(params['rainfall'])]

        parameters = pd.DataFrame(t_params)

    return parameters
