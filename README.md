# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for the Master of Business Analytics Programming class
* It contains code from Syndicate 1

### How do I get set up? ###

    * Ensure the following python modules are available:
    * cherrypy - python micro framework with mini http server
    * folium_plot - python wrapper for leaflet.js, a javascript mapping library
    * pandas
    * pickle - serialise/deserialise python objects
    * tkinter - python GUI
    * statsmodel

### How to start the application ###

In the terminal, go to the directory of the app and start up the webserver by:

    python ./main.py

It will most likely start serving on localhost:8080 (http://127.0.0.1:8080)

Then run:

    python ./gui_melb.py

to start the tkinter GUI interface.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
