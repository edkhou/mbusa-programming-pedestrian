#################################
# TO Do list
################################

# Colours
# Map values for to be quantiles
# Intergrate everything
# get aethetics all working
# -restric lengthe of date so its always right
# restict input as numbers
# Get webpage working to vary

# fix it so id doesnt need to be there


#######################
# Discuss
#######################
# should we report variance

# import libraries
import tkinter as tk
from datetime import *
import random
import webbrowser

# used in development
# import pandas
# import math
# import os

import melb_util as util
import pedestrian_model as model

# Return lambda ensures that function is run only when button is pressed

# List to append errors in inputs
error =[]

def cbc(id, tex, err):
    return lambda: callback(id, tex, err)

# Inputs the """""""clear"""""" function


def clear2():
    return lambda: clear1()

# link to html map


def map2():
    return lambda: map1()

# Workhorse funcitons


# Puts output in text
def callback(id, tex, err):
    tex.delete("1.0",tk.END)
    err.delete("1.0",tk.END)
    # get day of week from the date wanted
    
    # cForm = datetime.strptime(E4.get(), "%d/%m/%Y")
    # wday = datetime.weekday(cForm)
    # wday = datetime.weekday(cForm)
    noSensors = E.get()
    temp = E3.get()
    rain = E2.get()
    cDate = E4.get()

    if validate_sensor(noSensors) == True and validate_temperature(temp) == True and validate_rainfall(rain) == True and validate_date(cDate) == True:

        parameters = {}
        parameters['weekday'] = util.get_weekday(E4.get())
        parameters['day_of_year'] = util.get_day_of_year(E4.get())
        parameters['solar'] = util.get_solar_value(E1.get())
        parameters['temperature'] = float(E3.get())
        parameters['rainfall'] = float(E2.get())


        # pass the parameters to the model to obtain predicted traffic
        # a dictionary where sensor number is key, predicted count as values
        # returns a tuple (sensor_number, [ped_count])
        pedestrian_predictions = model.get_pedestrian_count(parameters)

        # sort based on pedestrian count
        sorted_pedestrian_predictions = util.get_sensor_ranking(
            pedestrian_predictions)

        # prints out the value and sensor number for the amount of sensors selected
        for i in range(1, int(E.get()) + 1):
            # s = "Suggestion Number {} for {}: Go to spot {} which we predict will have {} foot traffic\n\n".format(
            #     i, E4.get(), ords[i], vv[i])

            s = "Suggestion {} for {}: Go to spot {} which we predict will have {:.2f} foot traffic\n\n".format(
                i,
                E4.get(),
                sorted_pedestrian_predictions[i][0],
                sorted_pedestrian_predictions[i][1][0][0])

            tex.insert(tk.END, s)
            tex.see(tk.END)
    else:
        err.insert(tk.END,error)
        err.see(tk.END)
        error[:] = []

# Clear all
def clear1():
    # cc = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    tex.delete("1.0",tk.END)
    err.delete("1.0",tk.END)
    E.delete(0,tk.END)
    E1.set(0)
    E2.delete(0,tk.END)
    E3.delete(0,tk.END)
    E4.delete(0,tk.END)

# Opens up the webpage
def map1():
    url = "http://localhost:8080/calculate?weekday={}&day_of_year={}&solar={}&temperature={}&rainfall={}".format(util.get_weekday(E4.get()), util.get_day_of_year(E4.get()), util.get_solar_value(E1.get()), int(E3.get()), int(E2.get()))
    webbrowser.open_new_tab(url)

# Testing Inputs 
def validate_sensor(noSensors):  
    if not noSensors.isdigit():
        error.append("Please enter valid number of sensors - Whole numbers only")
        return False
    elif 0 < int(noSensors)> 41:
        error.append("Data - available for 41 sensors only. Please enter valid number of sensors")
        return False
    elif not float(noSensors).is_integer():
        error.append("Please enter valid number of sensors - Whole numbers only")
        return False
    return True

def validate_temperature(temp):  
    if not temp.replace(".","",1).isdigit():
        error.append("Please enter valid temperature")
        return False
    return True

def validate_rainfall(rain):  
    if not rain.replace(".","",1).isdigit():
        error.append("Please enter valid rainfall")
        return False
    return True

def validate_date(cForm):  
    try: 
       datetime.strptime(E4.get(), "%d/%m/%Y")
    except ValueError:
        error.append("Please enter date in the format dd/mm/yyyy")
        return False
    return True



####################
# SETS UP GUI
####################

# sets up the app
top = tk.Tk()

# sets up the text box for the output and error display
tex = tk.Text(master=top, width=120, height=15)
err = tk.Text(master =top, width=120, height = 2, fg="Red")

# place it
tex.grid(row=1, column=2, rowspan=10)
err.grid(row=10, column=2, rowspan =10 )


# Input for how many sensors we want
tk.Label(top, text="How many sensors i want to see").grid(row=1, column=1)
E = tk.Entry(top, bd=5)
E.grid(row=2, column=1)

# Sunshine input as a decile
tk.Label(top, text="Level of sunshine").grid(row=5, column=1)
E1 = tk.Scale(top, from_=0, to=9, orient="horizontal")
E1.grid(row=6, column=1)

# rain in put
tk.Label(top, text="Rain in mm").grid(row=7, column=1)
E2 = tk.Entry(top, bd=5)
E2.grid(row=8, column=1)

# tempreture input
tk.Label(top, text="Max temp").grid(row=9, column=1)
E3 = tk.Entry(top, bd=5)
E3.grid(row=10, column=1)

# Date input

tk.Label(top, text="Date: dd/mm/yyyy").grid(row=11, column=1)
E4 = tk.Entry(top, bd=5)
E4.grid(row=12, column=1)

# is a public holiday checkbox
# var1 = tk.IntVar()
# tk.Checkbutton(top, text="Public holiday",
#                variable=var1).grid(row=13, column=1)

# Button which executes the function
bbutton = tk.Button(top, text="Tell me!", command=cbc(1, tex, err), activebackground="Blue").grid(row=14, column=1)

# button that executes the map link
tk.Button(top, text="Show me a map", command=map2()).grid(row=15, column=1)

# Button that clsoes the app
tk.Button(top, text='Exit', command=top.destroy,
          activebackground="Red").grid(row=16, column=1)

# BUtton that """""""""""""""clear""""""""""""
tk.Button(top, text="clear", command=clear2()).grid(row=17, column=1)

top.mainloop()
