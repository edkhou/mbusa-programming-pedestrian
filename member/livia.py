import pandas as pd
from pandas import DataFrame
from datetime import datetime
import numpy as np



####read 3 tables###
rainfall = pd.read_csv('IDCJAC0009_086338_1800_Data.csv',parse_dates=True,skip_blank_lines=True, usecols=['Rainfall amount (millimetres)','Year','Month','Day'],
                 header=0,index_col=False)
temp = pd.read_csv('IDCJAC0010_086338_1800_Data.csv', parse_dates=True,skip_blank_lines=True, usecols=['Maximum temperature (Degree C)','Year','Month','Day'],
                 header=0,index_col=False)
solar = pd.read_csv('IDCJAC0016_086338_1800_Data.csv', parse_dates=True,skip_blank_lines=True, usecols=['Daily global solar exposure (MJ/m*m)','Year','Month','Day'],
                 header=0,index_col=False)
####convert year, month, date in to one column 

rainfall['Date'] = pd.to_datetime(rainfall.Year*10000 + rainfall.Month*100 + rainfall.Day, format='%Y%m%d')
del rainfall['Year']
del rainfall['Month']
del rainfall['Day']


temp['Date'] = pd.to_datetime(temp.Year*10000 + temp.Month*100 + temp.Day, format='%Y%m%d')

del temp['Year']
del temp['Month']
del temp['Day']

solar['Date'] = pd.to_datetime(solar.Year*10000 + solar.Month*100 + solar.Day, format='%Y%m%d')

del solar['Year']
del solar['Month']
del solar['Day']


###merge 3 tables and drop row with no value

df=pd.merge(rainfall,temp,on=['Date'],how='inner')
df1=pd.merge(df,solar,on=['Date'],how='inner')
df2=df1.dropna()



###aggregate ped_count#####
ped = pd.read_csv('Pedestrian_Counts.csv',parse_dates=True,infer_datetime_format=True,skip_blank_lines=True, usecols=['Date_Time','Sensor_ID','Hourly_Counts'],header=0)
ped.rename(columns={'Hourly_Counts':'Counts'},inplace=True)
# get convert datetime to date
temp = pd.DatetimeIndex(ped['Date_Time'])
ped['Date'] = temp.date
del ped['Date_Time']
sum_count=(ped.groupby([ped['Date'],ped['Sensor_ID']]).sum())

bw = df2.reset_index()
pw = sum_count.reset_index()

#convert pandas date type to python datetype
bw["Date"]=bw.Date.apply(lambda d: d.date())

###join ped_count with the 3 tables 
final_1=pd.merge(pw,bw,on=['Date'],how='left')
final_1=final_1.dropna()


###join sensor location table
location = pd.read_csv('Pedestrian_Sensor_Locations.csv',skip_blank_lines=True, usecols=['Sensor ID','Location Type'],
                 header=0,index_col=False)
final_2=pd.merge(final_1,location,left_on=['Sensor_ID'],right_on =['Sensor ID'],how='left')
final_2=final_2.dropna()


###add day of the week column 
final_2['Date']=pd.to_datetime(final_2['Date'])
final_2['day of the week']=final_2['Date'].dt.dayofweek
header=['Date','day of the week','Sensor_ID','Counts','Rainfall amount (millimetres)','Maximum temperature (Degree C)','Daily global solar exposure (MJ/m*m)','Location Type']

##print to csv
final_2.to_csv("Final_Livia.csv", header=True,columns=header) 


