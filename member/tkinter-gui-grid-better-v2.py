#################################
###TO Do list
################################

###Colours
###Map values for to be quantiles
###Intergrate everything
###get aethetics all working
###-restric lengthe of date so its always right
###restict input as numbers
### Get webpage working to vary

###fix it so id doesnt need to be there


#######################
#########Discuss
#######################
#####should we report variance

#import libraries
import tkinter as tk
from datetime import *
import random
import webbrowser

###used in development
#import pandas
#import math
#import os

###Return lambda ensures that function is run only when button is pressed

#Gets the string
def cbc(id, tex):
    return lambda : callback(id, tex)

#Inputs the """""""clear"""""" function
def clear2(id,tex):
    return lambda : clear1(id,tex)

#link to html map
def map2():
    return lambda : map1()

#########Workhorse funcitons


#Puts output in text
def callback(id, tex):

    #get day of week from the date wanted
    cForm=datetime.strptime(E4.get(),"%d/%m/%Y")
    wday=datetime.weekday(cForm)

#empty dictionary
    vals={}


###loops out values for all the sensors
    for i in range(42):
        outputval=.66*E1.get()+11*int(E2.get())+.32*int(E3.get())+1000*var1.get()+100000*wday+1000*random.random()
        vals[i]=outputval
    
    #sorts them to get order
    ords=sorted(vals, key=vals.get,reverse=True)
#### values in order
    vv=[vals[x] for x in ords]

#####prints out the value and sensor number for the amount of sensors selected
    for i in range(1,int(E.get())+1):    
        s="Suggestion Number {} for {}: Go to spot {} which we predict will have {} foot traffic\n\n".format(i,E4.get(),ords[i],vv[i])

        tex.insert(tk.END, s)
        tex.see(tk.END)   


####really ghetto way of clearing the screen
def clear1(id,tex):
    cc="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    tex.insert(tk.END,cc)
    tex.see(tk.END)

####Opens up the webpage
def map1():
    webbrowser.open_new_tab("file:///C:/Users/Owner/AppData/Local/Programs/Python/Python35-32/melb.html")
          

####################
#### SETS UP GUI
####################


####sets up the app
top = tk.Tk()

## sets up the text box for the output
tex = tk.Text(master=top,width=120,height=15)

####place it
tex.grid(row=1,column=2,rowspan=10)


####Images, not needed any more with the htmls
#photo = tk.PhotoImage(file="test.gif")
#label = tk.Label(master=top,image=photo)
#label.image = photo # keep a reference!
#label.grid(row=1,column=3,rowspan=10)


##creates a frame to put all the entries into 
#top = tk.Frame()
#top.pack(side=tk.LEFT)



##Input for how many sensors we want
tk.Label(top, text= "How many sensors i want to see").grid(row=1,column=1)
E=tk.Entry(top, bd =5)
E.grid(row=2,column=1)

##check specfic sensor--DO THIS LATER
#tk.Label(top, text= "Or i want to check a spefic sensor").grid(row=3,column=1)
#QQ=tk.Entry(top, bd =5)
#QQ.grid(row=4,column=1)

####Sunshine input as a decile
tk.Label(top, text= "Level of sunshine").grid(row=5,column=1)
E1=tk.Scale(top, from_=0, to=10, orient="horizontal")
E1.grid(row=6,column=1)

####rain in put
tk.Label(top, text= "Rain in mm").grid(row=7,column=1)
E2 = tk.Entry(top, bd =5)
E2.grid(row=8,column=1)

#tempreture input
tk.Label(top,text="Max temp").grid(row=9,column=1)
E3=tk.Entry(top,bd=5)
E3.grid(row=10,column=1)

#Date input

tk.Label(top,text="Date: dd/mm/yyyy").grid(row=11,column=1)
E4=tk.Entry(top,bd=5)
E4.grid(row=12,column=1)

#is a public holiday checkbox
var1 = tk.IntVar()
tk.Checkbutton(top, text="Public holiday", variable=var1).grid(row=13,column=1)

###Button which executes the function
bbutton=tk.Button(top,text="Tell me!", command=cbc(1, tex),activebackground="Blue").grid(row=14,column=1)


####button that executes the map link
tk.Button(top,text="Show me a map",command=map2()).grid(row=15,column=1)


###Button that clsoes the app
tk.Button(top, text='Exit', command=top.destroy,activebackground="Red").grid(row=16,column=1)



###BUtton that """""""""""""""clear""""""""""""
tk.Button(top,text="clear",command=clear2(1,tex)).grid(row=17,column=1)

top.mainloop()
