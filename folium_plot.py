import folium
import csv
import os

'''
This method will take a dictionary of sensor:pedestrian_count pairs
and create points on the map for each. 

No value is returned. Instead an HTML page is saved for loading
by the web page. 
'''
def plot_melb(pedestrian_count_1, pedestrian_count_2={}):
    # read the csv file containing sensor details such as 
    # coordinates and descriptions
    with open(os.getcwd() + '/assets/Pedestrian_Sensor_Locations.csv', 'r') as f:
        reader = csv.reader(f)
        sensorloc = {}
        row = 1
        for row in reader:
            sensorloc[int(row[0])] = row[1:]

    # pre-define the focus of the map to Melbourne
    melb_map = folium.Map(location=[-37.81, 144.98], zoom_start=14)

    # append the pedestrian count to sensorloc
    for k, v in sensorloc.items():
        if k in pedestrian_count_1:
            v.append(int(pedestrian_count_1[k][0]))
            v.append(pedestrian_count_1[k][1])
        elif k in pedestrian_count_2:
            v.append(int(pedestrian_count_2[k][0]))
            v.append(pedestrian_count_2[k][1])

    # create a CircleMarker for each sensor we have data on
    for k, v in sensorloc.items():
        folium.CircleMarker([v[7], v[8]], popup="{} {}".format(v[1], v[-2]),
                            radius=40,
                            color=v[-1], 
                            fill_color=v[-1]).add_to(melb_map)

    # save the generated HTML for display
    melb_map.save('public/melb.html')
