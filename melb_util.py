from datetime import datetime
import calendar

# create list of colours for sensors
# colouring based on order of sensors, red for number 1 highest ped. count
COLOURS = ["#00ff00", "#ffa500", "#0000ff", "#00008b"]
SOLAR_DECILES = 35.1 / 10
SOLAR_EXPOSURE = {0: 0,
                  1: int(1 * SOLAR_DECILES),
                  2: int(2 * SOLAR_DECILES),
                  3: int(3 * SOLAR_DECILES),
                  4: int(4 * SOLAR_DECILES),
                  5: int(5 * SOLAR_DECILES),
                  6: int(6 * SOLAR_DECILES),
                  7: int(7 * SOLAR_DECILES),
                  8: int(8 * SOLAR_DECILES),
                  9: int(9 * SOLAR_DECILES)
                  }


'''
given a dictionary of sensor:count pair, sort based on the value of count
returns a list of tuples [(sensor id, count)]
'''
def get_sensor_ranking(pedestrian_count):
    sorted_pedestrian_count = sorted(pedestrian_count.items(),
                                     key=lambda x: float(x[1][0]),
                                     reverse=True)
    return sorted_pedestrian_count

'''
given a list of sensors and pedestrian count,
assign a colour to the sensor based on the count

this helps differentiate sensors that are below or above the average 
'''
def append_colour_rankings(pedestrian_count):
    # max_count = max(pedestrian_count)
    # min_count = min(pedestrian_count)
    average_count = sum([pair[1][0]
                         for pair in pedestrian_count]) / len(pedestrian_count)

    for i in range(len(pedestrian_count)):
        if i == 0:
            pedestrian_count[i][1].append(COLOURS[0])
        elif (pedestrian_count[i][1][0] <= average_count):
            pedestrian_count[i][1].append(COLOURS[1])
        else:
            pedestrian_count[i][1].append(COLOURS[2])

    return pedestrian_count

# create a dictionary from a list


def get_dict_from_tuple_list(pedestrian_count):
    return {i[0]: i[1] for i in pedestrian_count}

'''
method to convert a decile value from the GUI (0-10)
to a solar value based on the range observed from the Melbourne dataset
'''
def get_solar_value(value):
    if value in SOLAR_EXPOSURE:
        return SOLAR_EXPOSURE[value]
    return False

'''
method that takes a Python Date object and 
returns respective day of the week (0-6 where 0 = Monday)
'''
def get_weekday(date):
    return calendar.day_name[datetime.strptime(date, '%d/%m/%Y').date().weekday()]

'''
method that takes a Python Date object and
returns the day of the year (1-366)
'''
def get_day_of_year(date):
    return datetime.strptime(date, '%d/%m/%Y').date().timetuple().tm_yday
