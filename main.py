import cherrypy
import pedestrian_model as model
import plot_melb as melb
import melb_util as util
import folium_plot as fp
import os, os.path


class Melbourne(object):

    # This is the index method that gets called upon loading the main page
    def index(self):
        # on start, unpickle the file containing the models
        is_load_success = model.load_models()

        return open("index.html")

    # exposed function that CherryPy will use to handle the calculate URL path
    @cherrypy.expose
    def calculate(self, weekday, day_of_year, solar, temperature, rainfall):
        # create a dictionary to hold the values
        parameters = {}
        parameters['weekday'] = weekday
        parameters['day_of_year'] = day_of_year
        parameters['solar'] = solar
        parameters['temperature'] = temperature
        parameters['rainfall'] = rainfall

        # pass the parameters to the model to obtain predicted traffic
        # a dictionary where sensor number is key, predicted count as values
        # returns a tuple (sensor_number, [ped_count])
        pedestrian_predictions = model.get_pedestrian_count(parameters)

        # sort based on pedestrian count
        sorted_pedestrian_predictions = util.get_sensor_ranking(pedestrian_predictions)

        # append colour based on pedestrian count
        coloured_pedestrian_predictions = util.append_colour_rankings(sorted_pedestrian_predictions)

        # convert back to a dictionary
        coloured_pedestrian_predictions = util.get_dict_from_tuple_list(coloured_pedestrian_predictions)

        # map plotting using bokeh (deprecated)
        # melb.get_map(pedestrian_predictions)

        # map plotting using folium/leaflet
        fp.plot_melb(coloured_pedestrian_predictions)

        # redirect the user to the index page
        raise cherrypy.HTTPRedirect("/")
        
    # exposed function to handle the Shutdown Server link
    @cherrypy.expose
    def shutdown(self):
        cherrypy.engine.exit()

    index.exposed = True

# CherryPy configuration, sets up directorys from where static
# content can be served from
if __name__ == '__main__':
     conf = {
         '/': {
             'tools.sessions.on': True,
             'tools.staticdir.root': os.path.abspath(os.getcwd())
         },
         '/static': {
             'tools.staticdir.on': True,
             'tools.staticdir.dir': './public'
         }
     }

# Initiate the webserver
cherrypy.quickstart(Melbourne(), '/', conf)
